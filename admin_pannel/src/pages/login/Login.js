import React from "react";
import userservice from "../../services/userService";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { ToastContainer, toast } from "react-toastify";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import "react-toastify/dist/ReactToastify.css";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: null,
    };
    this.userv = new userservice();
    this.props.handlerLoginParent(false);
    this.schema = Yup.object({
      email: Yup.string().required().email(),
      password: Yup.string()
        .matches(new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"))
        .required(),
    });
  }

  submitLogin(values, actions) {
    actions.setSubmitting(false);
    console.log(values);
    this.userv
      .login(values)
      .then((response) => {
        this.props.handlerLoginParent(true);
        this.setState({ loading: false, redirect: true });
      })
      .catch((e) => {
        console.log(e, "login");
        toast.error(e);
        actions.setStatus({
          email: "This is email already exists.",
          pswrd: "This is incorrect",
        });
        this.props.handlerLoginParent(false);
        this.setState({ loading: false, redirect: false });
      });
  }

  handleUsernameChange(event) {
    this.setState({ email: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }
  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/userlist" />;
    }

    return (
      <div className="login text-center">
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={this.schema}
          onSubmit={this.submitLogin.bind(this)}
          render={({ values, handleChange, errors, touched, handleBlur, handleSubmit }) => (
            <form className="form-signin" onSubmit={handleSubmit}>
              <img
                className="mb-4 logo"
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHkAtwMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAACAwQFAAEGB//EADsQAAEDAgQDBQYDBwUBAAAAAAEAAgMEEQUSITFBUWEGEyJxgTKRobHB0RQzQgcVI3Ky4fBSYoLC8TX/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMABAX/xAAgEQACAgMBAAIDAAAAAAAAAAAAAQIRAxIhMRNRIkFh/9oADAMBAAIRAxEAPwDzlilRgWUNjtE9kmi7ketFoMkZ7FZMW22Ud7rPusdJcIth2Q6Cx0RygAKNEcpTJJNFv0ZPgt1kqwuikcklymybY4xi1wFthbfKQsgdm0K3KyxvyRSCvs2Iml91qaEJ8DbhSvwzpbNY0ucdgBqjqPraKyJtlKAJFgjno56STLUQvjPC43XU4Z2RmqaNk0k7WOc0ODct7X5rKIE1H1nFSjVKLV1dL2WrKvEpqSW0Pc2L32uDfa3P6JlR2JrBV08NHK2Zsl873eHu7cwt8Un1CtqzjixaEa63GuxtZhtO+obKyeKMXflBDgOdlzmRLLG4+g1TIbmaoo47lOkbZMhAQUTKIh7LNUORupVhPxUJ41WkhZrpH7u62nhqxDUnqOD0bZCCoodqizopjKQ8m5TGhRmu11Tg/RMmMmMOiEv1Ws90BOqIWzTnXSnHVNLUp26mxGOhfopLnZmgqCwG6kM2TpjRfCXSuJcGNBc4mwAFyV6D2Bo5IqyrNXTPjd3IMZkYRx1tf0XI9jBE7GgJQCe6dkvwdp9Lr0+Bv8M5NHt+CdMdz/HUr8ZpKeoY+GVjXMdpY81PpbCCw2MQ26E/dUuKSS3eHHW2vMKfhcxmpqd/MOYfO1/mFW0yHSVSu7xkcpFnSO8Xpc/QKsNY+OPvh7THE6HzVpADHTwaWsHn4WXOzHJh07jwBP8AUspUhosuqGqZWQzCY3Y6MNcD1uT8FwVBgBcGfiSS9w0YDb3n/ArnD3SNw672uIkcLRjeQ8G/VWEcBjkbShzTUSm80nBoG48gi3vTYylRxvaDBxQMjnjdeKRxaByNlUMNl6Xj+DxYqymp2zmGOK5LWszOJ2GnDiuQxzAThUjTFIZYSQ3M61w7kbaKc8dO14NFlM6LM26gztyq5sBGVV1QuSpSQZoh5gFiB41WJbIbC2kowUsLd0oljmlMa6yilxWB6wVKiXnsjYbqIHprX2RsZSJTiAFFkdYonSXCQ910W7DKRJiIIVzgmB4hjUpjoIc2X2nuNmt8yqKmcvVP2V1dO2gqKZjg2rEhe4O/U2wsR8v/AFNjVsydIr6f9n+N0EkdVHU0fexuzBt328r5V1tNI/IGSWZMNN9+is66u7ptpGkC2ulx7wqKch7zLRva4j2mPdcO6K2lIybYFUA95OQZhpb6J+EU5p5304N45B3kPQjdvv8Amo7Z46o2B7uZvtCQWPr9051V3NOSABPEQ9gJt4hwPmLj1SN0Fqy6dGHUzbbDMCPkuZqaQz0ophvPIAejQAXH5j1U6k7TUlTHJkdYE5m38j/ZQ4qxlRXGmjcfCwNJB2vYu/6j3rJ2LTSJNJTZnCeFjTYFlMDswbOkPyCOCnDA7uHWB1kqnaE/y8h19ynudE2E7CE2HhH5hGzQOI6epQOpXTxmeuc2mo26hrzp5nmfgFZcQtiaVrqgmOjuyH9c+t3fy34dVxvbfEoH1UeH0bg6KmvncDe7/wCyu8Wx41bHUOBh7ITo+oA8TvL7+5c8OzDZBmLqi/p9kJSbVIrD7ZzrpfCokxuFe4lgj6WF0rHOdG3cOFiFSvYueSr0o+lfI3VYnzCyxTIuJW3W7pQKLMls59gnGyDNqhc5LzaoWCyW1yLOVHa5FmRsZMaZChDkolYCtYNidRN72qhjJNpHtaSNxc20Xs+EYfRUcDY8OpTG4AXlBAc49TuV4ths7Ketp5pQSyORriBvYFe14PjTZ6ZklLPAYnbWYG/GyvhfpWHSXPLUtFnNz9C1V78j35pKRzHf6mXBVhU1tYWnLTB/kWuuqx1bVk60LLdWtCo5FtTKqKCWM3kGcbZtHtPQrlqytrZK1lBGWySSOyseXZSPPoPRdNNVOEbjLBTxaa3fb4ArkpH00WMRVU4aKcBwc9odlALSL3Avx4Lnzyag3H0V8QYkpIS5sVJBK0PBbO2d47wafM3W4a2KGuNXSukbSyZRIx5LnRGx0Fr+E30O5uqmIvdEyOQyNfYaNuHdCNNb6/BMjjldK2MwPM00jXCEMyksH6utzcWttdc0Vq4uL6c8ZybpnquEVDHsbKGNY4jSWrP9MYN/eR5FSaihp614fWRVdc5p072zYx5NNh8FXYPIxkAa91RSkb2hBt6tup0sNNOP/rNd5yL1KQ/LGiNsLbR01FA0bAuv8gly1MQFnvpbfymyhy0NHH4n112/zgfVVFZjeB4dfLOyeUfpjPeH4aD1T7KI6ZG7V1EVHQSTRU7XsnBjDgDlBI3APkvOnyi5+is+0eOT4vOHOBjgZ7EV726nqqFzuK48s9mNtRuR9ysUd8mqxTJuRFAWHZGBZC7dKQFuQBMclhAAbdkdkLEwIhAK20aoiLrYFgtQaNq37O/vKTEY4MHdKKqY5Wtjda/O/C1uaqV6F+yjuYZ6qpP52jL8Ws3+fyTwVyKR/h2g7P1VLhrWYhUNra46nu42sDelxw6ndUjcDrJ5iwSNA45QNPVd3JUfi4xDTeza7n/pHW/FV85ZECwZQ1ozEu2HV30C64xT9KqUn6QMO7PUUFnPH4iRovnlNwOoB0A6pWM0MBo3zxxhxcQyEEfmOOgPl9lMim/GFp8Qpb+FpNnTu5not4ZN+98WbUA5qSkuIiNnu2Lh0voOg6rNID4ctT9lMT7zIyqc0UuW0mUd4PDsHb28W3MK47OdnGUuIVMNSe8nDiWvebl7d9/W/v5LsoDGzM+ws66qcYlMLKOvhdlf+WXdRcNPvBH/ACUo4oRdpdEtllGIu7DXsBy6bWc3oqrFaZgBkMLaiI76WePupcVZHidL+Jpzkmb4ZY+R5H6FV01dYkbcHD7roSvo0VZUPpKCY3jaxzTyVbinZinq4ny0gEdQBpbQO6EfVWGIRxRk1TM4ubvyfOyXHXRwwuklqo+7GpL7tKnkploxR5tVwljiHCxBsR1VbLpcK7xScVNTNM0WD3lw9SqSfdczRHJ6RXbrFtwWJCRo6ICjcluQFYLkHFGUHFAVhhGEARBEIwC6O2iBpTEUOgbKZhmI1GGVInpXBrtiCLhw5FRCgJWumC6Pc+xuNS13ZxlZWOYx73uuRfRrTYC3PQoKyV1RKTOwiFpu2AusT/ueeHkvJ8Ex+vwhrxRTNDX7se3MAeY5FTIMXr8WxKkhq6pxifM0GNvhbvyG/quiORUWjL7O2qa6bGKhuH4e8mOTwz1IFhkH6W8h1XWYW+HD6LOwBkcTCfQaD6qmoKdtNSAQMHfTXa23DWyi9tMSFFQQ4XTfmy5XSc2sadPebfFWf4xtjNW+HZwyAUkZH+k/IqixWTvuz74wTmBeR0IIeE+CuYaGIk6kG3XdUdbWWwlwYQCS63uARlSiGK4Jp6yanqIK6A2bKMsreHqptTWxVgc5l2zM0c1246Hn5qFQhstGWu0a8XtyKp+1wyYcythcWSse27mmxv7J+iXZqNg86WMtaG5myEBoHHiF56Sxk0jYrFjXENPS+iypxauqIzHNUucw7jK0X9bKG19lzSnbBLJZPdJdqgzm5RGTRIkddK2TbAO6xDdYlENOSymOQFAVgFDxRFaQFbNhEFpqIIhGNOqaEkJmZFDJm3GyQ86opHJJdcpWzNjmOsE+Gd0MrJYzZ7HBzT1ChgosyKYLPRKX9ophoi0YfeqHseMZB1J3Q00jMUf+8Z5mudIM8jnGwHMdALALz8ORZzlLbnKdxdV+aT9KxytHpeF9oaWsjkhbJkMLjbNpmbzCq8fxOMRw0MTw8+28g7cR/nRcPmvvqn0r/wCMPIpcuVyhRnlbVHXYN2ljpw+jrjljB8Eo4dD90vtRjdNUUJoqaQSudIHOc3Vthrv5gLlJ3fxnFJLlo5HpQPkdUMc5aa9KLlq+qSxLJGdKe66DMtXWs1m7ra0sRAGUCIoUDAlDxRIeKAgQRhAiWCmGFsnRaWnIjC5CljdE/dCEgrYS2tLfBEwQK3dCFiIQ0ynNpmpKKL81qD8MNqfzEm6ObdJQj4YIlCSsQpjBAraBbWNYfBYtDZYiE//Z"
                alt=""
                height="72"
              />
              <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
              <Form.Group>
                <Form.Control
                  type="text"
                  value={values.email}
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isValid={touched.email && !errors.email}
                  placeholder="Email"
                />
                <ErrorMessage name="email">{(msg) => <div className="err_below">{msg}</div>}</ErrorMessage>
              </Form.Group>
              <Form.Group>
                <Form.Control
                  type="password"
                  value={values.password}
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isValid={touched.password && !errors.password}
                  placeholder="Password"
                />
                <ErrorMessage name="password">
                  {(msg) => (
                    <div className="err_below">
                      {
                        "Password length should be >= 8. it must contain at least one small and capital alphabet, numbers and special characters."
                      }
                    </div>
                  )}
                </ErrorMessage>
              </Form.Group>

              <div className="checkbox mb-3">
                <label>{/* <input type="checkbox" value="remember-me" /> Remember me */}</label>
              </div>
              <button className="btn btn-lg btn-primary btn-block setbgcolor" type="submit">
                Sign in
              </button>
              <p className="mt-5 mb-3 text-muted">&copy;2019-2020</p>
            </form>
          )}
        />

        <ToastContainer />
      </div>
    );
  }
}
