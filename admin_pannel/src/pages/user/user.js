import React from "react";
import { Container, Nav, Navbar, Button } from "react-bootstrap";
import userservice from "../../services/userService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Formik, ErrorMessage } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";

const axios = require("axios");

export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.userId = props.match.params.id;
    this.state = { user: null, redirect: false, errorMsg: "" };
    this.userServ = new userservice();
    if (this.userId) {
      this.userServ.getUser(this.userId).then(
        (response) => {
          this.setState({ user: response });
        },
        (error) => {
          alert("Opps! Something went wrong not able to fetch User  details.");
        }
      );
    }
  }
  submitStudentForm(values, actions) {
    actions.setSubmitting(false);
  }

  render() {
    return (
      <Formik
        // validationSchema={this.schema}
        initialValues={this.state.user}
        enableReinitialize={true}
        onSubmit={this.submitStudentForm.bind(this)}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => (
          <div className="address addresslist">
            <Container>
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col sm={3}>
                    <h3>User Detail's</h3>
                  </Col>
                  <Col sm={3} style={{ marginTop: "10px" }}>
                    {/* <Link to={"/student/course/list"} >Student Course</Link> */}
                  </Col>
                </Row>

                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>First Name*</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.first_name}
                        name="first_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.first_name && !errors.first_name}
                      />
                      <ErrorMessage name="first_name">{(msg) => <div className="err_below">{msg}</div>}</ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.last_name}
                        name="last_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.last_name && !errors.last_name}
                      />
                      <ErrorMessage name="last_name">{(msg) => <div className="err_below">{msg}</div>}</ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Email</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.email}
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.email && !errors.email}
                      />
                      <ErrorMessage name="last_name">{(msg) => <div className="err_below">{msg}</div>}</ErrorMessage>
                    </Form.Group>
                  </Col>
                  <Col sm={12} md={6}>
                    <Form.Group>
                      <Form.Label>Role</Form.Label>
                      <Form.Control
                        type="text"
                        value={values.role}
                        name="role"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.role && !errors.role}
                      />
                      <ErrorMessage name="role">{(msg) => <div className="err_below">{msg}</div>}</ErrorMessage>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col sm={12} md={4}></Col>
                  <Col sm={12} md={4}>
                    <button disabled className="btn btn-lg btn-primary btn-block setbgcolor" type="submit">
                      Save
                    </button>
                  </Col>
                </Row>
              </Form>
            </Container>
          </div>
        )}
      />
    );
  }
}
