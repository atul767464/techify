import React from "react";
import Table from "react-bootstrap/Table";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Pagination from "react-bootstrap/Pagination";
import { Link } from "react-router-dom";
import Userservices from "../../services/userService";
import SearchBar from "../../components/searchbar";
import Button from "react-bootstrap/Button";
import { DropdownButton, Dropdown } from "react-bootstrap";
export default class userList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
    };
    this.search = {
      start: 0,
      perPage: 10,
      searchTxt: "",
      searchField: "",
    };
    this.userServ = new Userservices();
  }

  componentDidMount() {
    this.getUserList();
  }

  getUserList() {
    this.userServ.listUser().then(
      (response) => {
        if (!response) {
          return (response = []);
        }
        this.setState({
          userList: response,
          totalCount: response,
        });
      },
      (error) => {
        this.setState({ userList: [], totalcount: 0 });
      }
    );
  }

  render() {
    let data = [];
    for (let i = 0; i < this.state.userList.length; i++) {
      // if (this.state.userList[i].is_deleted === false) {
      if (window.user.user.role == "admin") {
        data.push(
          <tr key={"user" + this.state.userList[i]._id}>
            <td>{this.search.start + i + 1}</td>
            <td>
              <Link to={{ pathname: "/user/edit/" + this.state.userList[i]._id }}>
                {this.state.userList[i]["first_name"]}
              </Link>
            </td>
            <td>{this.state.userList[i]["email"]}</td>

            <td>{this.state.userList[i]["_id"]}</td>
            <td>{this.state.userList[i].role}</td>
          </tr>
        );
      } else if (window.user.user.role == "editor") {
        data.push(
          <tr key={"user" + this.state.userList[i]._id}>
            <td>{this.search.start + i + 1}</td>
            <td>{this.state.userList[i]["first_name"]}</td>
            <td>{this.state.userList[i]["email"]}</td>
            <td>{this.state.userList[i]["role"]}</td>
          </tr>
        );
      }

      // }
    }
    return (
      <div className="address addresslist">
        <Row>
          <Col sm={12}>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>User Id</th>
                  <th>Role</th>
                </tr>
              </thead>
              <tbody>{data}</tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
