import util from "../utils/util";

export default class UserService {
  login(value) {
    return util
      .sendApiRequest("/user/login", "POST", true, value)
      .then((response) => {
        localStorage.setItem("user", JSON.stringify(response));
        window.user = response;
        return window.user;
      })
      .catch((e) => {
        if (e.includes("data")) {
          let err = JSON.parse(e);
          throw err.err.toString();
        }
        throw e;
      });
  }

  logout() {
    localStorage.removeItem("user");
    window.user = null;
  }

  getUser(id, withCourse) {
    return util.sendApiRequest("/user/" + id + (withCourse ? "/1" : ""), "GET", true).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  editUser(user) {
    return util.sendApiRequest("/user", "PUT", true, user).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }

  listUser(data) {
    return util
      .sendApiRequest("/user/list", "GET", true)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        throw err;
      });
  }
  addUser(user) {
    return util.sendApiRequest("/user", "POST", true, user).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }
  deleteUser(_id) {
    let id = { _id };
    return util.sendApiRequest("/user/delete", "POST", true, id).then(
      (response) => {
        return response;
      },
      (error) => {
        throw new Error(error);
      }
    );
  }
}
