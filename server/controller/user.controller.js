const User = require("../models/user");
const bcrypt = require("bcrypt");
const UserServ = require("../services/user.service");
const utils = require("../utils/utils");
const jwt = require("jsonwebtoken");

module.exports = {
  add: async function (req, res, next) {
    let result = await UserServ.add(req.body, next);
    utils.sendResponse(result, req, res);
  },

  listAll: async function (req, res, next) {
    let result = await UserServ.listAll(req.currUser, next);

    utils.sendResponse(result, req, res);
  },

  getDetail: async function (req, res) {
    console.log(req.params.id);
    let result = await UserServ.getDetail(req.params.id);
    utils.sendResponse(result, req, res);
  },

  login: async function (req, res, next) {
    const { email, password } = req.body;
    let result = await UserServ.login({ email, password, next });
    if (result) utils.sendResponse(result, req, res);
  },
};
