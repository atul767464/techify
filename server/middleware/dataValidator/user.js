//Insert your joi schema here
const Joi = require("@hapi/joi");

module.exports.userAdd = async (req, res, next) => {
  //   console.log(req.body, "middle");
  const { first_name, last_name, email, password } = req.body;
  try {
    let schema = Joi.object({
      first_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required()
        .error((errors) => new Error("first name is required Field")),
      last_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .error(() => new Error("last name is required Field")),
      password: Joi.string()
        .pattern(new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"))
        .error(
          (errors) =>
            new Error(
              `Password length should be >= 8. 
              it must contain at least one small and capital alphabet,
              numbers and special characters.`
            )
        ),

      email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } }),
    });

    let validationData = schema.validate({
      first_name: first_name,
      last_name: last_name,
      email: email,
      password: password,
    });
    if (validationData.error !== undefined) {
      throw new Error(validationData.error.message);
    }
    next();
  } catch (e) {
    next(e);
  }
};

module.exports.userLogin = async (req, res, next) => {
  console.log(req.body, "middle");
  const { email, password } = req.body;
  try {
    let schema = Joi.object({
      password: Joi.string().required(),
      email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } }),
    });

    let validationData = schema.validate({
      email: email,
      password: password,
    });
    if (validationData.error !== undefined) {
      throw new Error(validationData.error.message);
    }
    next();
  } catch (e) {
    next(e);
  }
};
