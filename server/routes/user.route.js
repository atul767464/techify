const express = require("express");
const router = express.Router();
const userController = require("../controller/user.controller");
const dataVal = require("../middleware/dataValidator/user");
const auth = require("../middleware/auth");

router.route("/").post(dataVal.userAdd, userController.add);

router.route("/list").get(auth, userController.listAll);

router.route("/:id").get(auth, userController.getDetail);

router.route("/login").post(dataVal.userLogin, userController.login);

module.exports = router;
