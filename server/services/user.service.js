const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
module.exports = {
  add: async function (user, next) {
    const saltRounds = 10;

    let result;
    try {
      let userData = await User.findOne({ email: user.email });
      if (userData) {
        throw new Error("Email Already Taken");
      }
      let salt = bcrypt.genSaltSync(saltRounds);
      let hash = bcrypt.hashSync(user.password, salt);
      user.password = hash;
      result = await new User(user).save();
      return result;
    } catch (error) {
      next(error);
    }
  },

  listAll: async function (curruser, next) {
    try {
      if (curruser.role == "editor") {
        let result = await User.find({ _id: curruser._id });
        return result;
      }
      let result = await User.find();
      return result;
    } catch (error) {
      next(error);
    }
  },
  getDetail: async function (id, next) {
    try {
      result = await User.findById(id);
      return result;
    } catch (error) {
      next(error);
    }
  },
  login: async function ({ email, password, next }) {
    console.log(password, email);
    try {
      let user = await User.findOne({ email: email }).select("+password");
      console.log(user);
      if (!user) {
        throw new Error("Email not found in our Database");
      }
      let check = await bcrypt.compare(password, user.password);
      user.password = undefined;
      if (!check) {
        throw new Error("Email or Password is invalid");
      }
      let token = await jwt.sign({ email: user.email, userId: user._id }, process.env.JWT_KEY);
      return {
        user,
        token,
      };
    } catch (err) {
      next(err);
    }
  },
};
