module.exports = {
  sendResponse: function (result, req, res) {
    res.status(200).send(result);
  },
};
